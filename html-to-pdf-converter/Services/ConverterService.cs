﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PuppeteerSharp;

namespace html_to_pdf_converter.Services
{
    public interface IConverterService
    {
        Task<Byte[]> GeneratePdf(string htmlBody);
    }
    
    public class ConverterService : IConverterService
    {
        public async Task<Byte[]> GeneratePdf([FromBody] string htmlBody)
        {
            await new BrowserFetcher().DownloadAsync(BrowserFetcher.DefaultRevision);
            var browser = await Puppeteer.LaunchAsync(new LaunchOptions
            {
                Headless = true,
            });
            var page = await browser.NewPageAsync();
            await page.SetContentAsync(htmlBody);
            return await page.PdfDataAsync();
        }
    }
}
﻿using System.Threading.Tasks;
using html_to_pdf_converter.Services;
using Microsoft.AspNetCore.Mvc;

namespace html_to_pdf_converter.Controllers
{
    [ApiController]
    [Route("api/{controller}")]
    public class ConverterController : ControllerBase
    {
        //private readonly IConverterService converterService;
        
        [HttpPost]
        public async Task<IActionResult> HtmlToPdfConverter([FromBody] string htmlToConvert)
        {
            var converterService = new ConverterService();
            return Ok(await converterService.GeneratePdf(htmlToConvert));
        }
    }
}